package com.tuliomir.desafioandroidtuliomir;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;
import com.tuliomir.desafioandroidtuliomir.service.DribbbleService;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_details)
public class DetailsActivity extends Activity implements ShotFragment.OnShotFragmentInteractionListener {

    @Extra
    long shot_id;

    @FragmentById(R.id.detailsShotFragment)
    ShotFragment detailsShotFragment;

    private Context context;

    @ViewById
    ImageView playerAvatar;

    @ViewById
    TextView playerName;

    @ViewById
    TextView txtShotDescription;

    @Override
    protected void onStart() {
        super.onStart();

        context = this.getApplicationContext();
        obterDetalhesDoDribbble();
    }

    @Background
    void obterDetalhesDoDribbble() {
        DribbbleShot shot_object = DribbbleService.obterDetalhesShot(this.shot_id);

        preencherDetalhes(shot_object);
    }

    @UiThread
    void preencherDetalhes(DribbbleShot shot_object) {
        if (shot_object != null) {
            // Preenchendo dados do Fragment
            this.detailsShotFragment.init(shot_object, context);
            this.detailsShotFragment.preencheImagem();

            // Preenchendo dados da Activity de Details
            Picasso.with(this.context).load(shot_object.getPlayer().getAvatar_url()).into(this.playerAvatar);
            this.playerName.setText(String.valueOf(shot_object.getPlayer().getName()));

            // Tratando a descrição
            String strDescription = String.valueOf(Html.fromHtml(shot_object.getDescription()));
            this.txtShotDescription.setText(strDescription);
        }
    }

    /**
     * Este método é referente a um Fragment, uma decisão errada que foi tomada no início do
     * desenvolvimento. Não é mais necessário e seria removido em uma próxima refatoração.
     * TODO: Remover esta interface (e reconstruir o Fragment da maneira correta)
     * @param shot_id Obsoleto
     */
    @Override
    @Deprecated
    public void onFragmentShotClicked(long shot_id) {

    }
}
