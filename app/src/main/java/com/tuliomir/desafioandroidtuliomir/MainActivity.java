package com.tuliomir.desafioandroidtuliomir;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListView;

import com.tuliomir.desafioandroidtuliomir.adapter.ShotsAdapter;
import com.tuliomir.desafioandroidtuliomir.helpers.EndlessScrollListener;
import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {

    @ViewById
    ListView mainShotsListView;

    @Bean
    ShotsAdapter adapter;

    @AfterViews
    void bindAdapter() {
        mainShotsListView.setAdapter(adapter);

        mainShotsListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                adapter.loadNextPage(page);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @ItemClick
    void mainShotsListViewItemClicked(DribbbleShot shot) {
        Intent intent = DetailsActivity_.intent(getApplicationContext())
                                        .shot_id(shot.getId())
                                        .get();
        startActivity(intent);
    }
}
