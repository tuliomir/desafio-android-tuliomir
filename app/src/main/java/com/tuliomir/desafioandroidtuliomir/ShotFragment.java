package com.tuliomir.desafioandroidtuliomir;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;
import com.tuliomir.desafioandroidtuliomir.ui.ShotImageView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

/**
 *
 * Este fragment (gerado automaticamente pelo Android Studio) foi fruto de uma decisão incorreta
 * tomada durante o início do desenvolvimento, quando desejava implementar uma simples lógica
 * de Layout (atualmente a classe ShotItemView).
 *
 * Deixo esta classe aqui pois ainda é utilizada na Activity de Detalhes, e a refatoração iria levar
 * mais tempo do que o que tenho disponível. Para uma melhor explicação, vide o Wiki:
 * {@link https://bitbucket.org/tuliomir/desafio-android-tuliomir/wiki/Home}
 *
 */

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.tuliomir.desafioandroidtuliomir.ShotFragment.OnShotFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShotFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@EFragment
public class ShotFragment extends Fragment {
    // the fragment initialization parameters
    private static final String ARG_SHOT_IMAGE_URL = "shot_image_url";
    private static final String ARG_SHOT_TITLE = "shot_title";
    private static final String ARG_SHOT_LIKES_COUNT = "shot_likes_count";

    private long shot_id;
    private String shot_image_url;
    private String shot_title;

    private int shot_likes_count;
    private OnShotFragmentInteractionListener mListener;

    private Context context = null;

    @ViewById(R.id.shotImageView)
    ShotImageView shotImageView;

    @ViewById(R.id.txtShotTitle)
    TextView txtShotTitle;

    @ViewById(R.id.txtLikesCount)
    TextView txtLikesCount;
    private boolean imagemDirecionaParaDetalhes;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param image_url The shot's image URL
     * @param likes_count The shot's likes count
     * @return A new instance of fragment ShotFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShotFragment newInstance(String image_url, String shot_title, int likes_count) {
        ShotFragment fragment = new ShotFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SHOT_IMAGE_URL, image_url);
        args.putString(ARG_SHOT_TITLE, shot_title);
        args.putInt(ARG_SHOT_LIKES_COUNT, likes_count);
        fragment.setArguments(args);
        return fragment;
    }

    public ShotFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shot_image_url = getArguments().getString(ARG_SHOT_IMAGE_URL);
            shot_title = getArguments().getString(ARG_SHOT_TITLE);
            shot_likes_count = getArguments().getInt(ARG_SHOT_LIKES_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shot, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnShotFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnShotFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Define que o clique da imagem irá direcionar para a tela de detalhes, caso o parâmetro receba
     * valor true.
     * @param imagemDirecionaParaDetalhes True define que imagem direciona para activity de detalhes
     */
    public void defineLinkDetalhes(boolean imagemDirecionaParaDetalhes) {
        this.imagemDirecionaParaDetalhes = imagemDirecionaParaDetalhes;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnShotFragmentInteractionListener {
        public void onFragmentShotClicked(long shot_id);
    }

    /**
     * Enquanto não tenho tempo para estudar a melhor forma de obter o Contexto no ciclo de vida do
     * Fragment, passo esta responsabilidade para a própria Activity.
     * TODO: Estudar ciclo de vida do Fragment para definir a melhor forma de obter este conteúdo.
     * @param context Contexto da Aplicação
     */
    public void init(DribbbleShot shot, Context context) {
        if (null != shot) {
            this.shot_image_url = shot.getImage_url();
            this.shot_title = shot.getTitle();
            this.shot_likes_count = shot.getLikes_count();
            this.shot_id = shot.getId();
        }
        this.context = context;
    }







    /**
     * Métodos do Negócio
     */

    @UiThread
    void preencheImagem() {
        Picasso.with(this.context).load(this.shot_image_url).into(this.shotImageView);
        this.txtShotTitle.setText(String.valueOf(this.shot_title));
        this.txtLikesCount.setText(String.valueOf(this.shot_likes_count));
    }

    @Click
    void shotImageView() {
        if (!this.imagemDirecionaParaDetalhes) {
            return;
        }

        if (mListener != null) {
            mListener.onFragmentShotClicked(this.shot_id);
        }
    }

}
