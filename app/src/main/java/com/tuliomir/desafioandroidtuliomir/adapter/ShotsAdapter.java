package com.tuliomir.desafioandroidtuliomir.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;
import com.tuliomir.desafioandroidtuliomir.service.DribbbleService;
import com.tuliomir.desafioandroidtuliomir.ui.ShotItemView;
import com.tuliomir.desafioandroidtuliomir.ui.ShotItemView_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tuliomiranda on 3/23/15.
 *
 * Adaptador para exibição de DribbbleShot's em um ListView
 */
@EBean
public class ShotsAdapter extends BaseAdapter {

    List<DribbbleShot> shots;

    @Bean(DribbbleService.class)
    DribbbleService dribbbleService;

    @RootContext
    Context context;

    @AfterInject
    void initAdapter() {
        setShots(new ArrayList<DribbbleShot>());
        carregarListaShotsMaisPopulares();
    }

    @Override
    public int getCount() {
        return shots.size();
    }

    @Override
    public DribbbleShot getItem(int position) {
        return shots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ShotItemView shotItemView;
        if (convertView == null) {
            shotItemView = ShotItemView_.build(context);
        } else {
            shotItemView = (ShotItemView) convertView;
        }

        shotItemView.bind(getItem(position));

        return shotItemView;
    }

    public void setShots(List<DribbbleShot> shots) {
        this.shots = shots;
    }

    public List<DribbbleShot> getShots() {
        return this.shots;
    }


    /**
     * Carrega a primeira página dos Shots mais populares, inicializando a aplicação.
     */
    void carregarListaShotsMaisPopulares() {
        loadNextPage(1);
    }

    /**
     * Carrega a próxima página de Shots mais populares.
     * @param page Página a ser obtida da API Dribbble.
     */
    @Background
    public void loadNextPage(int page) {
        List<DribbbleShot> objShots = DribbbleService.obterShotsMaisPopulares(page);
        popularValoresObtidos(objShots);
    }

    /**
     * Adiciona os valores do parâmetro à lista já existente de Shots populares.
     *
     * Obs.: Este método possui uma lógica muito simples de "append" ao final da lista já existente,
     * o que poderia causar maus usos. Ponto a debater com os possíveis desenvolvedores no projeto.
     * @param objShots Lista contendo os shots a serem adicionados ao ListView
     */
    @UiThread
    void popularValoresObtidos(List<DribbbleShot> objShots) {
        for(DribbbleShot shot : objShots) {
            this.shots.add(shot);
        }

        this.notifyDataSetChanged();
    }
}
