package com.tuliomir.desafioandroidtuliomir.service;

import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;

import org.androidannotations.annotations.EBean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by tuliomiranda on 3/22/15.
 *
 * Classe de acesso à API do Dribbble.
 */
@EBean
public class DribbbleService {
    private static final String URL_DRIBBLE_POPULAR = "http://api.dribbble.com/shots/popular?page=";
    private static final String URL_DRIBBLE_SHOT_DETAILS = "http://api.dribbble.com/shots/";

    /**
     * Realiza GET sobre a URL passada como parâmetro, retornando uma String com o resultado.
     * @param url Endereço Web a ser obtido
     * @return String contendo o retorno do GET
     */
    private static String obterJsonDaURL(String url) {
        String result = null;

        // Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

        // Add the String message converter
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        try {
            // Make the HTTP GET request, marshaling the response to a String
            result = restTemplate.getForObject(url, String.class, "Android");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return result;
    }

    /**
     * Realiza chamada à API do Dribbble, obtendo os Shots mais populares, passando uma página
     * como parâmetro.
     * @param pagina Número da página a ser obtida
     * @return Conteúdo JSON obtido da API Dribbble
     */
    private static String obterListaShotsPopularesPorPagina(int pagina) {
        String numeroPagina = String.valueOf(pagina);

        return obterJsonDaURL(URL_DRIBBLE_POPULAR + numeroPagina );
    }


    // Métodos Públicos

    /**
     * Obtém os Shots mais populares no Dribbble, filtrados por página
     * @param pagina Número da página a ser obtida
     * @return Lista com os Shots mais populares, para a página especificada
     */
    public static List<DribbbleShot> obterShotsMaisPopulares(int pagina) {
        List<DribbbleShot> listaRetorno;

        String result = obterListaShotsPopularesPorPagina(pagina);
        //TODO: Tratar caso de página acima do limite, retornando JSON com "404"
        listaRetorno = JSONParser.paginaMaisPopulares(result);

        return listaRetorno;
    }

    /**
     * Obtém os detalhes de um Shot com ID específico, através da API Dribbble.
     * @param id_shot Propriedade "id" do Shot
     * @return Objeto DribbbleShot contendo os detalhes do Shot
     */
    public static DribbbleShot obterDetalhesShot(long id_shot) {
        DribbbleShot result;

        // Obtendo JSON
        String url_shot = URL_DRIBBLE_SHOT_DETAILS + String.valueOf(id_shot);
        String shotJSON = obterJsonDaURL(url_shot);

        // Validando os dados recebidos
        //TODO: Esta validação deveria ser muito melhor definida.
        if (null == shotJSON) {
            return null;
        }

        // Construindo o objeto de retorno
        result = JSONParser.obterDetalhesShot(shotJSON);

        return result;
    }
}
