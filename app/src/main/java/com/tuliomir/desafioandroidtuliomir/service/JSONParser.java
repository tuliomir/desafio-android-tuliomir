package com.tuliomir.desafioandroidtuliomir.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;

import java.io.IOException;
import java.util.List;

/**
 * Created by tuliomiranda on 3/22/15.
 *
 * Classe responsável por interpretar os JSON's recebidos e transformá-los em objetos para a App.
 */
public class JSONParser {

    /**
     * Obtém os objetos DribbbleShot a partir do conteúdo JSON de uma página de populares vinda da
     * API Dribbble.
     * @param jsonData Conteúdo JSON contendo uma página de mais populares
     * @return Lista de Shots referentes à página de mais populares
     */
    public static List<DribbbleShot> paginaMaisPopulares(String jsonData) {
        List<DribbbleShot> listaRetorno = null;

        // Inicializando variáveis
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode;

        try {
            // Encontrando o nodo de "Shots"
            rootNode = objectMapper.readTree(jsonData);
            JsonNode shotsNode = rootNode.path("shots");

            // Obtendo a lista de Shots a partir deste nodo
            TypeReference<List<DribbbleShot>> typeRef = new TypeReference<List<DribbbleShot>>() {};
            listaRetorno = objectMapper.readValue(shotsNode.traverse(), typeRef);

        } catch (IOException e) {
            //TODO: Realizar um tratamento de erro devido, com lançamento de exceções.
            e.printStackTrace();
        }

        return listaRetorno;
    }

    /**
     * Obtém os detalhes de um Shot, obtido a partir da API de detalhes do Shot do Dribbble.
     * @param jsonData Conteúdo JSON contendo os detalhes do shot no nó raiz
     * @return Objeto DribbbleShot contendo os detalhes do Shot
     */
    public static DribbbleShot obterDetalhesShot(String jsonData) {
        DribbbleShot objRetorno = null;

        // Inicializando variáveis
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode;

        try {
            // Obtendo os detalhes do Shot a partir do nó raiz
            rootNode = objectMapper.readTree(jsonData);
            objRetorno = objectMapper.readValue(String.valueOf(rootNode), DribbbleShot.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return objRetorno;
    }
}
