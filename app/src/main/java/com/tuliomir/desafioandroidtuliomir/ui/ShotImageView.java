package com.tuliomir.desafioandroidtuliomir.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by tuliomiranda on 3/22/15.
 */
public class ShotImageView extends ImageView {


    public ShotImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        /*
         * Definindo Aspect Ratio do ImageView para 800x600
         */
        int width = getMeasuredWidth();
        int height;
        height = (int) Math.round( width * 0.75 );

        setMeasuredDimension(width, height);
    }
}
