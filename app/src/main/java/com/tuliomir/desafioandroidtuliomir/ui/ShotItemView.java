package com.tuliomir.desafioandroidtuliomir.ui;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tuliomir.desafioandroidtuliomir.R;
import com.tuliomir.desafioandroidtuliomir.model.DribbbleShot;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

/**
 * Created by tuliomiranda on 3/23/15.
 */
@EViewGroup(R.layout.shot)
public class ShotItemView extends LinearLayout {
    private final Context context;

    @ViewById
    ShotImageView shotImageView;

    @ViewById
    TextView txtShotTitle;

    @ViewById
    TextView txtLikesCount;

    public ShotItemView(Context context) {
        super(context);
        this.context = context;
    }

    @UiThread
    public void bind(DribbbleShot shot) {
        Picasso.with(this.context).load(shot.getImage_url()).into(this.shotImageView);
        this.txtShotTitle.setText(String.valueOf(shot.getTitle()));
        this.txtLikesCount.setText(String.valueOf(shot.getLikes_count()));
    }

}
